# Waterous Web Dashboard Application #

The Waterous Web Dashboard Demo is an application that uses the HDC Restful APIs in a custom-built UI that is an added layer to 
Helix Device Cloud


### Setup ###

Clone the repository, there is only 1 branch (master)

Install [Ruby](https://www.ruby-lang.org/en/documentation/installation/#rubyinstaller)

`npm install ruby --save`

Install Compass

`gem install compass`

After these have been installed, cd to project directory 'waterous_app' to install project dependencies. We will also install grunt as our task runner.

`npm install`

`bower install`

These should install all dependencies. If the next step doesnt work (grunt), then install grunt and grunt-cli with

`bower install grunt`

`bower install grunt-cli`

### Build & Development ###

After everything is installed,  we will use grunt as our task runner.

cd to the project directory and 

run `grunt serve` to serve the web page locally (http://localhost:9000)

`grunt build` will compile the project into the 'dist' folder. Contents of this folder is used to deploy to your server of choice


### Python Agent ###

Python agent is located in /python/Waterous_Winter_Release_BuildX.zip

Upload Waterous_Winter_Release_BuildX.zip to device

unzip the .zip file, it will extract 3 files: waterous_deploy.sh,waterous_run.sh, waterous_winter.zip

run `bash ./waterous_deploy.sh` to handle permissions and extract/copy over necessary files.

run `waterous_run.sh` to run the agent, or manually run `waterous_iot.py` to enable the python agent.

The applicataion will register with HDC and start sending telemetry. It will auto tag itself as 'tellurus' and be viewable from the web application



### Contact ###

Contact Jacob Cao (jacob.cao@windriver.com) if you have any questions!
