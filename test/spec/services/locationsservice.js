'use strict';

describe('Service: locationsService', function () {

  // load the service's module
  beforeEach(module('helixDeviceCloudApp'));

  // instantiate service
  var locationsService;
  beforeEach(inject(function (_locationsService_) {
    locationsService = _locationsService_;
  }));

  it('should do something', function () {
    expect(!!locationsService).toBe(true);
  });

});
