'use strict';

describe('Service: devicesService', function () {

  // load the service's module
  beforeEach(module('helixDeviceCloudApp'));

  // instantiate service
  var devicesService;
  beforeEach(inject(function (_devicesService_) {
    devicesService = _devicesService_;
  }));

  it('should do something', function () {
    expect(!!devicesService).toBe(true);
  });

});
