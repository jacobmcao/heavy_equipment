'use strict';

describe('Service: deviceLogsService', function () {

  // load the service's module
  beforeEach(module('helixDeviceCloudApp'));

  // instantiate service
  var deviceLogsService;
  beforeEach(inject(function (_deviceLogsService_) {
    deviceLogsService = _deviceLogsService_;
  }));

  it('should do something', function () {
    expect(!!deviceLogsService).toBe(true);
  });

});
