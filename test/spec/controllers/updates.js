'use strict';

describe('Controller: UpdatesCtrl', function () {

  // load the controller's module
  beforeEach(module('helixDeviceCloudApp'));

  var UpdatesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UpdatesCtrl = $controller('UpdatesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(UpdatesCtrl.awesomeThings.length).toBe(3);
  });
});
