'use strict';

describe('Directive: efficiencyPanel', function () {

  // load the directive's module
  beforeEach(module('helixDeviceCloudApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<efficiency-panel></efficiency-panel>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the efficiencyPanel directive');
  }));
});
