'use strict';

describe('Directive: updatesPanel', function () {

  // load the directive's module
  beforeEach(module('helixDeviceCloudApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<updates-panel></updates-panel>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the updatesPanel directive');
  }));
});
