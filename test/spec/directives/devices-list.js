'use strict';

describe('Directive: devicesList', function () {

  // load the directive's module
  beforeEach(module('helixDeviceCloudApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<devices-list></devices-list>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the devicesList directive');
  }));
});
