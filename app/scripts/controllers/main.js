'use strict';

/**
 * @ngdoc function
 * @name helixDeviceCloudApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the helixDeviceCloudApp
 */


angular.module('helixDeviceCloudApp').controller('MainCtrl', function ($scope, $rootScope, devicesService, $route, $routeParams, $location) {

        $scope.genericToggle = 0;
        $scope.deviceDetailsOpen = false;
        $scope.fullDashboard = false;
        $scope.device = null;
        $scope.mainPage = true;
        $scope.deviceId = parseInt($routeParams.deviceid);

        //when view is rendered, start api request chain
        $scope.$on('$viewContentLoaded', function() {
          devicesService.thingSearch();

          if ($rootScope.globalAlertsToggle == 0){
            devicesService.thingSearch();
          }
          else
          {}
        });

        //when api calls are done loading, check url params
        $scope.$on('linesLoaded', function() {
        });

        $scope.showDeviceDetails = function(id) {
          //console.log('showDeviceDetails:Main:', id);
          $scope.$apply();
          $scope.device = devicesService.getDevice(id);
          $rootScope.$broadcast('showDeviceDetails', id);

        if($scope.device) {
            $scope.deviceDetailsOpen = true;
          } else {
            $scope.deviceDetailsOpen = false;
          }
        };


        $scope.deviceVisible = function() {
          return $scope.deviceDetailsOpen;
        };
});
