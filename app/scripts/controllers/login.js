/**
 * Created by jcao on 5/24/2016.
 */
'use strict';

angular.module('helixDeviceCloudApp').controller('LoginCtrl', function ($scope, $location, $rootScope, $q, $http) {




  $rootScope.globalSession = "";

  $scope.submitForm = function(){
    $scope.nextLogin();
  };

  $scope.nextLogin = function(){
    var result;
    var dataBlock = {
      "auth" : {
        "command" : "api.authenticate",
        "params" : {
          "username": $scope.username,
          "password": $scope.password
        }
      }
    };
    $.ajax({
      url: 'https://core-api.hdcstg.net/api',
      ContentType: "application/x-www-form-urlencoded",
      async: false,
      type: 'POST',
      data: JSON.stringify(dataBlock),
      success: function(data, textStatus, jqXHR) {
        result = data;
        if(result.hasOwnProperty('auth')){
          $rootScope.globalSession = result.auth.params.sessionId;
          $rootScope.loggedIn = true;
          $location.path('/devices');
        }
        else if (result.success == false)
        {
          alert("Login Failed - Wrong Username/Password");
        }
        else {
          alert("Incorrect Inputs");
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert("Request Failed");
      }

    });
  };




  $scope.testProxy = function(){
    //var url = 'http://172.21.5.4:5000/v2.0/tokens';
    var url = 'http://172.21.5.4:5000/v2.0/tokens';
    var dataBlock = {"auth":{"passwordCredentials":{"username": "hdcuser", "password": "hdcuser"}, "tenantName": "hdcdemo"}};


    /*

    $.ajax({
      url: url,
      type: 'POST',
      ContentType: "application/json",
      async: true,
      data: JSON.stringify(dataBlock),
      beforeSend: function(xhr) {
        xhr.setRequestHeader('Content-Type' , 'application/json');
        xhr.setRequestHeader('X-Container-Meta-Access-Control-Allow-Origin' , 'http://localhost:9000');
      },
      success: function(data, textStatus, jqXHR) {
        alert(textStatus);
        console.log(data.access.token.id);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });


    */




    $.ajax({
      async: true,
      crossDomain: true,
      url: url,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      processData: false,
      data: JSON.stringify(dataBlock),
      beforeSend: function(xhr) {
        xhr.setRequestHeader('Content-Type' , 'application/json');
        xhr.setRequestHeader('http-proxy' , 'http://128.224.2.4:9090');
      },
      success: function(data, textStatus, jqXHR) {
        alert(textStatus);
        console.log(data.access.token.id);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });





  };



  $scope.titanium= function(){
    console.log("testlogin");
    var url = 'http://172.21.5.4:5000/v2.0/tokens';
    var dataBlock = {"auth":{"passwordCredentials":{"username": "hdcuser", "password": "hdcuser"}, "tenantName": "hdcdemo"}};

    var d = $q.defer();
    var req = {
      method: 'POST',
      url: url,
      data: dataBlock,
      headers: {
        'Content-Type': 'application/json',
        'Accept': '*/*'
      }
    };
    $http(req).then(function(resp) {
      d.resolve();
    }, function(resp) {
      console.log(resp);
      d.reject();
    });
    return d.promise;
  };


  $scope.serverDetails= function(){
    //var proxy = '?callback=http://128.224.2.4:9090';
    var url = '/servers/detail';

    console.log($rootScope.instanceData.access.token.id);
    console.log($rootScope.instanceData.access.serviceCatalog[1].endpoints[0].publicURL);

    $.ajax({
      url: $rootScope.instanceData.access.serviceCatalog[1].endpoints[0].publicURL + url,
      type: 'GET',
      ContentType: "application/json",
      headers:{
        'X-Auth-Token' : $rootScope.instanceData.access.token.id
      },
      beforeSend: function(xhr) {
        xhr.setRequestHeader('Content-Type' , 'application/json');
        //xhr.setRequestHeader('X-Auth-Token' , $rootScope.instanceData.access.token.id);
      },
      success: function(data, textStatus, jqXHR) {
        alert(textStatus);
        //$scope.instanceData = data;


      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  };



  $scope.testServer= function(){
    var HttpProxyAgent = require('http-proxy-agent');
    var request = require('request');
    var proxy = 'http://128.224.2.4:9090';
    var agent = new HttpProxyAgent(proxy);
    var dataBlock = {"auth":{"passwordCredentials":{"username": "hdcuser", "password": "hdcuser"}, "tenantName": "hdcdemo"}};
    request({
      uri: "",
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        'Accept': '*/*'
      },
      agent: agent,
      timeout: 10000,
      maxRedirects: 10,
      body: dataBlock
    }, function(error, response, body) {
      console.log("Error" + error);
      console.log("Response: " + response);
      console.log("Body: " + body);
    });
  };




});




