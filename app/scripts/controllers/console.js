'use strict';

/**
 * @ngdoc function
 * @name helixDeviceCloudApp.controller:ConsoleCtrl
 * @description
 * # ConsoleCtrl
 * Controller of the helixDeviceCloudApp
 */
angular.module('helixDeviceCloudApp').controller('ConsoleCtrl', function ($scope, $uibModalInstance, $rootScope) {

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.dismiss = function() {
        $uibModalInstance.dismiss('dismiss');
    };


    //delete the session
  $scope.disconnectSession = function(){
    var tempID = "Basic " +  $rootScope.globalAuth ;
    var proxy2 = "api.helixdevicecloud.com";
    var newApi = "vd9pqa3yxv6famrmc75fe8a5";
    var tenant = "39921C64-C697-4D23-AF2B-979EFDC7340B";
    var session = $rootScope.sessionVar.sessionID;
    var url = 'https://' + proxy2 + '/rest/csp/v1/remote-login/' + tenant + "/" + session + "?api_key=" + newApi;

    $.ajax(url, {
      type: 'DELETE',
      beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", tempID);
      },
      success:function(data, textStatus, jqXHR) {
        console.log("success: " + JSON.stringify(data));
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert("failure: " + textStatus);
      }
    });
  };


});
