'use strict';

/**
 * @ngdoc overview
 * @name helixDeviceCloudApp
 * @description
 * # helixDeviceCloudApp
 *
 * Main module of the application.
 */
angular.module('helixDeviceCloudApp', [
    'ngAnimate',
    'ngRoute',
    'ngResource',
    'ngTouch',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'chart.js',
    'mp.colorPicker'
  ])
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        reloadOnSearch: false
      })
      .when('/devices/', {

        resolve: {
          "check": function($location, $rootScope) {
            if (!$rootScope.loggedIn) {
              $location.path('/');
            }
          }
        },

        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main',
        reloadOnSearch: false
      })
      .when('/devices/:deviceid', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main',
        reloadOnSearch: false
      })
      .otherwise({
        redirectTo: '/devices'
      });
  })

