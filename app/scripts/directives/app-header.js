'use strict';

/**
 * @ngdoc directive
 * @name helixDeviceCloudApp.directive:header
 * @description
 * # header
 */
angular.module('helixDeviceCloudApp')
  .directive('appHeader',[ '$rootScope', function ($rootScope) {
    return {
      	templateUrl: 'views/header.html',
      	restrict: 'E',
    	controllerAs : 'appHeader',
      controller: function($scope) {        
            $scope.dashboardVisible = false;
            
            $scope.showDashboard = function(state) {
             	$rootScope.$emit('showDashboard', state);
              	$scope.dashboardVisible = state;            
            };

            $scope.$on('hideDashboard', function(e, state) {            	
      				$scope.dashboardVisible = state;
      			});       
        },
      
    };
  }]);
