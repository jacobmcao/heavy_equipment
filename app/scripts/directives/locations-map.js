'use strict';

/**
 * @ngdoc directive
 * @name helixDeviceCloudApp.directive:locationMap
 * @description
 * # locationMap
 */
angular.module('helixDeviceCloudApp')
		.directive('locationsMap', function() {
				return {
						templateUrl: 'views/locations-map.html',
						restrict: 'E',
						controller: function($scope, $rootScope, devicesService, $interval, $uibModal, $window, $timeout) {





              // LINE GRAPH

              $scope.labels1 = ['','','','','Data','','','','',''];
              $scope.series = ['Temperature'];
              $scope.data1= [[1]];
              $scope.options ={
                scales: {
                  yAxes: [{
                    ticks: {
                      max: 100,
                      min: 0,
                    }
                  }]
                }
              };


              /*

              $scope.labels1 = ['','','','','Data','','','','',''];
              $scope.series = ['Temperature', 'Free Memory'];
              $scope.data1= [[1],[2]];
              $scope.options ={
                scales: {
                  yAxes: [{
                    ticks: {
                      max: 100,
                      min: 0,
                    }
                  }]
                }
              };

              */


// BAR CHART INFORMATION


                $scope.barLabels = ['Fan Speed', 'Free Memory', 'Temperature', 'Light'];
                $scope.barSeries = ['Telemetry'];
                $scope.barData = [
                  [1, 1, 1, 1]
                ];


              $interval(function() {
                if (!$scope.device) return;
                //console.log($scope.device.id);


                var dataBlock = {"auth" : {"sessionId" : $rootScope.globalSession},"1" : {"command" : "thing.search","params" : {"query": $scope.device.id,
                  "limit": 10,
                  "offset": 0,
                  "show": [
                    "id",
                    "key",
                    "name",
                    "defKey",
                    "defName",
                    "properties",
                    "alarms",
                    "attrs"
                  ],
                  "sort": "+name"}}};


                $.ajax({
                  url: 'https://core-api.hdcstg.net/api',
                  ContentType: "application/x-www-form-urlencoded",
                  async: false,
                  type: 'POST',
                  data: JSON.stringify(dataBlock),
                  success: function(data, textStatus, jqXHR) {
                    var dataChunk = data["1"].params.result;
                    //console.log(JSON.stringify(data));
                    //console.log(dataChunk[0].properties.fan.value);
                    //console.log(dataChunk[0].properties.free_memory.value);
                    //console.log(dataChunk[0].properties.temperature.value);
                    //console.log(dataChunk[0].properties.light.value);




                    var fan = dataChunk[0].properties.fan.value;
                    var free_memory = dataChunk[0].properties.free_memory.value;
                    var temperature = dataChunk[0].properties.temperature.value;
                    var light = dataChunk[0].properties.light.value;

                    $scope.temperatureData = Math.ceil(dataChunk[0].properties.temperature.value);
                    //$scope.memoryData = dataChunk[0].properties.free_memory.value;

                    //Line graph data
                    if ($scope.data1[0].length > 9)
                    {
                      $scope.data1[0].shift();
                      //$scope.data1[1].shift();
                    }

                    $scope.data1[0].push($scope.temperatureData);
                    //$scope.data1[1].push($scope.memoryData);


                    if ($scope.device.groups == 142)
                    {

                      $scope.barLabels = ['Fan Speed','Temperature', 'Light'];
                      $scope.barSeries = ['Telemetry'];
                      $scope.barData = [
                        [fan,temperature, light]
                      ];

                    }
                    else if ($scope.device.groups == 150)
                    {

                      $scope.barLabels = ['Free Memory'];
                      $scope.barSeries = ['Telemetry'];
                      $scope.barData = [
                        [free_memory]
                      ];
                    }

                    else if ($scope.device.groups == 155)
                    {

                      $scope.barLabels = ['Fan Speed', 'Light'];
                      $scope.barSeries = ['Telemetry'];
                      $scope.barData = [
                        [fan,light]
                      ];
                    }




                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                    console.log("")
                  }
                });


              }, 2500);




              $interval(function() {
                if (!$scope.device) return;
                //console.log($scope.device.id);
                var dataBlock = {"auth" : {"sessionId" : $rootScope.globalSession},"1" : {"command" : "thing.search","params" : {"query": $scope.device.id,
                  "limit": 10,
                  "offset": 0,
                  "show": [
                    "id",
                    "attrs"
                  ],
                  "sort": "+name"}}};

                $.ajax({
                  url: 'https://core-api.hdcstg.net/api',
                  ContentType: "application/x-www-form-urlencoded",
                  async: false,
                  type: 'POST',
                  data: JSON.stringify(dataBlock),
                  success: function(data, textStatus, jqXHR) {
                    var dataChunk = data["1"].params.result;
                    $scope.device.softwareModules = {};
                    $scope.device.softwareModules.friendlyName = "Firmware Update";
                    $scope.device.softwareModules.version = dataChunk[0].attrs.app_version.value +".0";
                    $scope.updatingDevice = false;


                    if ($scope.device.softwareModules.version == "1.0")
                    {
                      $scope.device.softwareModules.current = $scope.device.softwareModules.friendlyName + " " + dataChunk[0].attrs.app_version.value +".0";
                      $scope.device.softwareModules.available = $scope.device.softwareModules.friendlyName + " " + "2.0";
                      $scope.previousUpdates = true;
                      $scope.currentUpdates = false;
                      //console.log("1.0 " + $scope.device.softwareModules.current);
                    }

                    else if ($scope.device.softwareModules.version == "2.0")
                    {
                      $scope.device.softwareModules.current = $scope.device.softwareModules.friendlyName + " " + dataChunk[0].attrs.app_version.value +".0";
                      $scope.device.softwareModules.previous = $scope.device.softwareModules.friendlyName + " " + "1.0";
                      //console.log("2.0 " + $scope.device.softwareModules.current);
                      //console.log("Previous " + $scope.device.softwareModules.previous);
                      $scope.currentUpdates = true;
                      $scope.previousUpdates = false;
                    }

                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                    console.log("")
                  }
                });
              }, 1500);









              $scope.$on('showDeviceDetails', function(e, device) {
                $scope.device = devicesService.getDevice(device);
                $scope.parseMap();
                $scope.initialValues();
              });

              //Init Values
              $scope.initialValues = function(){

                  };


              $(".btn").mouseup(function(){
                $(this).blur();
              })


                $(".dropdown-menu li a").click(function(){
                $(this).parents(".dropdown").find('.btn').html($(this).text() +
                  ' <span class="caret"></span>');
                $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
                  //alert($(this).text());
              });




              $('#dropdownSelect1').each(function(){
                //$scope.selText1 =  $('#selected1').text($(this).text());
                //alert($scope.selText1);
              });

              $('#dropdownSelect2').each(function(){
                $scope.selText2 = $(this).text();
                //alert($scope.selText2);
              });


              $scope.testSelector = function (){
              };


              $("#launchExtra").click(function() {
                $(".nav-tabs .active, .tab-content .active").removeClass("active");
                $("#extra").addClass("active");
              });

              $("#launchExtra1").click(function() {
                $(".nav-tabs .active, .tab-content .active").removeClass("active");
                $("#extra1").addClass("active");
              });

              $("#launchExtra2").click(function() {
                $(".nav-tabs .active, .tab-content .active").removeClass("active");
                $("#extra2").addClass("active");
              });

              $("#launchExtra3").click(function() {
                $(".nav-tabs .active, .tab-content .active").removeClass("active");
                $("#extra3").addClass("active");
              });


              $scope.currentLocation = 1;
              $scope.chartColors = [ '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'];



							//set default map view
							$scope.$on('linesLoaded', function() {

							});

              $scope.parseMap = function(){
                //var deviceUUID = $scope.device.uuid;
                //var deviceUpper = deviceUUID.toUpperCase();
                var cities = [];

             /*   cities[0] =
                 {
                 place: 'Tellurus Device',
                 lat: $rootScope.globalLatitude[0],
                 long: $rootScope.globalLongitude[0]
                 };

                 */


                for (var g = 0; g < $rootScope.globalLatitude.length; g++){
                  if (!$scope.device) return;
                  cities[g] =
                  {
                    place : $rootScope.globalFriendly[g],
                    lat : $rootScope.globalLatitude[g],
                    long : $rootScope.globalLongitude[g]
                  };


                }



                var mapOptions = {
                  zoom: 3,
                  center: new google.maps.LatLng($rootScope.globalLatitude[$scope.device.identifier],$rootScope.globalLongitude[$scope.device.identifier]),
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                };


                $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
                $scope.markers = [];

                var infoWindow = new google.maps.InfoWindow();

                var createMarker = function (info){

                  var marker = new google.maps.Marker({
                    map: $scope.map,
                    position: new google.maps.LatLng(info.lat, info.long),
                    title: info.place
                  });

                  marker.content = '<div class="infoWindowContent">'  + info.lat + ' E,' + info.long +  ' N, </div>';

                  google.maps.event.addListener(marker, 'click', function(){
                    infoWindow.setContent('<h2>' + marker.title + '</h2>' +
                      marker.content);
                    infoWindow.open($scope.map, marker);
                  });
                  $scope.markers.push(marker);
                };

                for (var count = 0; count < cities.length; count++){
                  createMarker(cities[count]);

                }

                $scope.openInfoWindow = function(e, selectedMarker){
                  e.preventDefault();
                  google.maps.event.trigger(selectedMarker, 'click');
                };

              };

              $scope.testFunction = function(){
                $scope.parseMap()
              };






              $scope.gotoHDC = function() {
                var deviceID = $scope.device.id;
                $window.open('https://core-portal.hdcstg.net/things/view/' + deviceID , '_blank');
              };


              $scope.gotoLaunch = function() {
                var remoteURL = 'https://preview.hdcdevel.net/terminal/' + $scope.device.key;
                console.log(remoteURL);
                $window.open(remoteURL , '_blank');
              };


              $scope.createConsoleModal = function() {
                $scope.urlRetrieve();
                $uibModal.open({
                  animation: $scope.animationsEnabled,
                  templateUrl: 'views/console-modal.html',
                  controller: 'ConsoleCtrl',
                  windowClass: 'console-modal',
                  backdrop: 'static'
                });
                console.log("Console Button Clicked");
              };

              //retrieve url for sessionID
              $scope.urlRetrieve = function() {
                var deviceUUID = $scope.device.uuid;
                var device = deviceUUID.toUpperCase();
                var tempID = "Basic amFjb2J3YXRlcm91czpqYWNvYg==";
                var proxy2 = "api.helixdevicecloud.com";
                var newApi = "vd9pqa3yxv6famrmc75fe8a5";
                var tenant = "39921C64-C697-4D23-AF2B-979EFDC7340B";
                var url = 'https://' + proxy2 + '/rest/csp/v1/remote-login/' + tenant + '?deviceID=' + device + "&api_key=" + newApi;

                $.ajax(url, {
                  type: 'POST',
                  dataType: 'json',
                  data: { sessionName: 'Session' },
                  beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", tempID);
                  },
                  success: function(data, textStatus, jqXHR) {
                    console.log("success:" + JSON.stringify(data));
                    $('#url').attr('src', data.url);
                    $rootScope.sessionVar = data;
                    //console.log("SESSION ID URL:" + $rootScope.sessionVar.url);
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                    console.log("failure:" + textStatus);
                  }
                });
              };



              $scope.firmwareUpdate1 = function(){
                if (!$scope.device) return;
                $scope.deviceUpdating = true;
                $scope.previousStatus = "- Installing...";

                var dataBlock ={"auth" : {"sessionId" : $rootScope.globalSession},
                  "1" :
                  {"command" : "method.exec",
                    "params" :
                    {
                      "thingKey": $scope.device.key,
                      "method":"software_update",
                      "singleton": false,
                      "ackTimeout": 30,
                      "params":{
                        "ota_timeout":0,
                        "package": "iot_demo_methods-1.0.tar.gz"
                      }
                    }
                  }};
                var url = 'https://core-api.hdcstg.net/api';
                $.ajax({
                  url: url,
                  ContentType: "application/x-www-form-urlencoded",
                  type: 'POST',
                  data: JSON.stringify(dataBlock),
                  success: function(data, textStatus, jqXHR) {
                    $scope.previousStatus = "- Complete!";
                    $timeout(function(){
                      $scope.previousStatus = "";
                      $scope.deviceUpdating = false;
                    }, 5000);
                    //alert("Success! Firmware Updated to Version 1.0");
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                    $scope.deviceUpdating = false;
                  }
                });
              };

              $scope.firmwareUpdate2 = function(){
                if (!$scope.device) return;
                $scope.deviceUpdating = true;
                $scope.currentStatus = "- Installing...";
                var dataBlock ={"auth" : {"sessionId" : $rootScope.globalSession},
                  "1" :
                  {"command" : "method.exec",
                    "params" :
                    {
                      "thingKey": $scope.device.key,
                      "method":"software_update",
                      "singleton": false,
                      "ackTimeout": 30,
                      "params":{
                        "ota_timeout":0,
                        "package": "iot_demo_methods-2.0.tar.gz"
                      }
                    }
                  }};
                var url = 'https://core-api.hdcstg.net/api';
                $.ajax({
                  url: url,
                  ContentType: "application/x-www-form-urlencoded",
                  type: 'POST',
                  data: JSON.stringify(dataBlock),
                  success: function(data, textStatus, jqXHR) {
                    $scope.currentStatus = "- Complete!";
                    $timeout(function(){
                      $scope.currentStatus = "";
                      $scope.deviceUpdating = false;
                    }, 5000);
                    //alert("Success! Firmware Updated to Version 2.0");
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                    $scope.deviceUpdating = false;
                  }
                });
              };




              $scope.testProxy = function(){
                var url = 'http://172.21.5.4:8774/v2.1/11d81897337940099882b47782683622/servers/detail';
                $.ajax({
                  url: url,
                  ContentType: "application/json",
                  type: 'GET',
                  headers: {
                    'X-Auth-Token' : "57802ede44c0484690b6ec04b9fc0243"
                  },
                  success: function(data, textStatus, jqXHR) {
                    alert(textStatus);
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                  }
                });
              };










            }
				};
		});
