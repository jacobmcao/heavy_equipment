'use strict';

/**
 * @ngdoc directive
 * @name helixDeviceCloudApp.directive:deviceDetails
 * @description
 * # deviceDetails
 */
angular.module('helixDeviceCloudApp')
  .directive('deviceDetails', function() {
    //var $this = this;
    return {
      templateUrl: 'views/device-details.html',
      restrict: 'E',
      controller: function($scope, $location, $uibModal, devicesService, deviceLogsService, $interval, $rootScope, ticketsService, $window) {

        $scope.fullDetails = false;


        $scope.$on('showDeviceDetails', function(e, device) {
          $scope.setDevice(device);

        });

        $scope.setDevice = function(deviceId) {
          $scope.device = devicesService.getDevice(deviceId);
          $scope.checkDeviceStatus();
        };

        $scope.expandDeviceDetails = function() {
          $scope.fullDetails = !$scope.fullDetails;
        };

        $scope.gotoSupport = function() {
        };

        if ($scope.device) {
        }

        $scope.dismiss = function() {
          $uibModalInstance.dismiss('dismiss');
        };



      }
    };

  });
