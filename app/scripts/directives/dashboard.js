'use strict';

/**
 * @ngdoc directive
 * @name helixDeviceCloudApp.directive:dashboard
 * @description
 * # controlPanel
 */
angular.module('helixDeviceCloudApp')
		.directive('dashboard', function() {
				return {
						templateUrl: 'views/dashboard.html',
						restrict: 'E',
						controller: function($rootScope, $scope, $location, $timeout) {

								$scope.dashboardView = true;
								$scope.dashboardVisible = false;
								$scope.fullDashboard = false;
								$scope.panel5Collapsed = true;
              

								$scope.$on('showDashboard', function(e, state) {
										$scope.dashboardVisible = state;
								});

								/*
								 * Method to hide dashboard, set class
								 * vars to false and emit the method
								*/
								$scope.hideDashboard = function() {
										$rootScope.$emit('hideDashboard', false);
										$scope.fullDashboard = false;
										$scope.dashboardVisible = false;
								};

								/*
								 * "Link" method to link to other pages
								 * @param {strong} path as URL
								 * @param {object} event - ng-click event passed to method
								*/
								$scope.goTo = function(path, event) {
										if(event !== undefined) {
											// stop event propagation on links nested in
											// parent with ng-click attribute
											event.stopPropagation();
										}
										//hide dashboard
										$scope.hideDashboard();
										//timeout to allow dashboard to animate close
										$timeout(function() {
											$location.path(path, null).search("deviceid", null);
										}, 600);
								};

								/*
								* Expand Dashboard to wide/full view
								*/
								$scope.expandDashboard = function() {
										$scope.fullDashboard = !$scope.fullDashboard;
								};

						},
						controllerAs: 'dashboard'

				};

		});
