'use strict';

/**
 * @ngdoc directive
 * @name helixDeviceCloudApp.directive:devicesList
 * @description
 * # devicesList
 */
angular.module('helixDeviceCloudApp')
    .directive('devicesList', function() {
        return {
            templateUrl: 'views/devices-list.html',
            restrict: 'E',
            controller: function($scope, $rootScope, $location, devicesService, $route, $routeParams) {
                $scope.deviceShown = false;
                $scope.deviceCollapsed = true;
                $scope.groupCollapsed = true;

                //attempt to init on creation
                try{
                    init();
                } catch(e) {
                    //data not available
                }

                //listen for future changes
                $scope.$on('linesLoaded', function(e) {
                    init();
                });


                function init() {
                    $scope.groupedDevices = devicesService.getGroupedDeviceList();
                    var urlDeviceId = parseInt($routeParams.deviceid);
                    if(urlDeviceId) {
                        selectDevice(parseInt($routeParams.deviceid));
                    }
                }

                //if the devices is linked to directly
                /*
                 *  Method to broadcast that a devices Details is displayed
                 *  param: device - device id set on click
                 */
                $scope.showDeviceDetails = function(device) {
                    //console.log('List:showDeviceDetails:', device);

                    if($route.current.$$route.controller === "MainCtrl") {
                        $location.search("deviceid", device);
                    } else {
                        $location.path("/devices").search('deviceid', device);
                    }

                    if($scope.dashboardVisible) {
                        $scope.hideDashboard(false);
                    }


                    if (device) {
                        $rootScope.$broadcast('showDeviceDetails', device);
                        $scope.deviceDetailsOpen = true;
                        selectDevice(device);
                    } else {
                        //$scope.deviceDetailsOpen = false;
                    }
                };

                /*
                 * "Link" method to link to other pages
                 * @param {object} event - ng-click event passed to method
                */

                function selectDevice(deviceId) {
                    deselectDevices();
                    getDeviceById(deviceId).selected = true;
                }

                function deselectDevices() {
                    for(var i=0; i<$scope.groupedDevices.length; i++) {
                        //loop through devices on line
                        for(var j=0; j<$scope.groupedDevices[i].devices.length; j++) {
                            $scope.groupedDevices[i].devices[j].selected = false;
                        }
                    }
                }


                //finds a devices by id from any line
                function getDeviceById(id) {
                    var deviceData;
                    //loop through lines
                    for(var i=0; i<$scope.groupedDevices.length; i++) {
                        //loop through devices on line
                        for(var j=0; j<$scope.groupedDevices[i].devices.length; j++) {
                            if($scope.groupedDevices[i].devices[j].id == id) {
                                deviceData = $scope.groupedDevices[i].devices[j];
                            }
                        }
                    }
                    return deviceData;
                }
            }

        };
    });
