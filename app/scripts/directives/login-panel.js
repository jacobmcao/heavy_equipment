/**
 * Created by jcao on 5/24/2016.
 */
'use strict';

/**
 * @ngdoc directive
 * @name helixDeviceCloudApp.directive:alertsPanel
 * @description
 * # loginPanel
 */
angular.module('helixDeviceCloudApp').directive('loginPanel', function () {
    return {
      templateUrl: 'views/login.html',
      restrict: 'E'
    };
  });
