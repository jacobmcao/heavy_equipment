'use strict';

/**
 * @ngdoc service
 * @name helixDeviceCloudApp.devicesService
 * @description
 * # devicesService
 * Service in the helixDeviceCloudApp.
 */



angular.module('helixDeviceCloudApp').service('devicesService', function($http, $q, $rootScope) {

  $http.defaults.headers.common.Authorization = 'Basic amFjb2J3YXRlcm91czpqYWNvYg==';


		var $this = this;
    var alarmDataset = [];
    var lines;

  $rootScope.globalLatitude = [];
  $rootScope.globalLongitude = [];
  $rootScope.globalFriendly = [];


  // if device pushed up an id it would identify automatically
  $rootScope.globalAlertsToggle = 3;
  $rootScope.testCities = [];

		this.deviceGroups = [{
        "id": 1,
        "name": "GlobalRoot",
        "userFriendlyName": "Global Root",
        "type": 7,
        "parentId": 0,
        "description": "",
        "textPath": "GlobalRoot",
        "links": [{
          "rel": "self",
          "href": "1"
        }, {
          "rel": "parent",
          "href": "0"
        }]
      },
      {
      "id": 142,
      "name": "Equipment",
      "userFriendlyName": "Devices",
      "type": 4,
        "parentId": 0,
        "description": "General Thing Group for Heavy Equipment",
				"textPath": "GlobalRoot\\IotDevice",
				"links": [{
						"rel": "self",
						"href": "1"
				}, {
						"rel": "parent",
						"href": "0"
				}]
		},
      {
        "id": 150,
        "name": "Titanium VM ",
        "userFriendlyName": "Instance VM",
        "type": 4,
        "parentId": 0,
        "description": "",
        "textPath": "GlobalRoot\\IotDevice",
        "links": [{
          "rel": "self",
          "href": "1"
        }, {
          "rel": "parent",
          "href": "0"
        }]
      },
      {
        "id": 155,
        "name": "VSCADA",
        "userFriendlyName": "VSCADA",
        "type": 4,
        "parentId": 0,
        "description": "",
        "textPath": "GlobalRoot\\IotDevice",
        "links": [{
          "rel": "self",
          "href": "1"
        }, {
          "rel": "parent",
          "href": "0"
        }]
      }
    ];



  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }


  //use tellurus for EAR_WATEROUS
  //use waterous for DEMO


  this.thingSearch = function(){
    var dataBlock = {"auth" : {"sessionId" : $rootScope.globalSession},"1" : {"command" : "thing.search","params" : {"query": "heavy_equipment",
      "limit": 20,
      "offset": 0,
      "show": [
        "id",
        "key",
        "name",
        "ownerOrgId",
        "permission",
        "defKey",
        "defName",
        "gateway",
        "iccid",
        "esn",
        "meid",
        "imei",
        "imsi",
        "connected",
        "lastSeen",
        "lastCommunication",
        "loc",
        "proto",
        "remoteAddr",
        "data",
        "properties",
        "alarms",
        "attrs",
        "createdOn",
        "updatedOn",
        "apiCounts.daily",
        "apiCounts.monthToDate",
        "apiCounts.total",
        "storage",
        "varBillingPlanCode",
        "varVasPackageCode"
      ],
      "sort": "+createdOn"}}};

    $.ajax({
      url: 'https://core-api.hdcstg.net/api',
      ContentType: "application/x-www-form-urlencoded",
      async: false,
      type: 'POST',
      data: JSON.stringify(dataBlock),
      success: function(data, textStatus, jqXHR) {
        var deviceList = data["1"].params.result;
        //console.log(JSON.stringify(deviceList[0].loc.lat));

        for (var i = 0; i < deviceList.length; i++)
        {
          //deviceList[i].id = i + 1;
          //alert(deviceList.length);
          deviceList[i].locationId = 1;
          //[deviceType[i]]; hardcoded group number, added to newline of each element
          deviceList[i].identifier = i;
          //$rootScope.globalLatitude[i] = deviceList[i].loc.lat;
          //$rootScope.globalLongitude[i] = deviceList[i].loc.lng;
          //console.log($rootScope.globalLatitude[i]);
          //console.log($rootScope.globalLongitude[i]);
          //$rootScope.globalFriendly[i] = deviceList[i].name;
          deviceList[i].softwareModules = {};
          deviceList[i].softwareModules.friendlyName = "Firmware Update";
          deviceList[i].softwareModules.value = deviceList[i].attrs.app_version.value;


          if (deviceList[i].name.indexOf("Equipment") !== -1){
            deviceList[i].groups = [142];
            console.log(deviceList[i].name + " 142");
          }
          else if (deviceList[i].name.indexOf("Titanium") !== -1) {
            deviceList[i].groups = [150];
            console.log(deviceList[i].name + " 150");
          }
          else if (deviceList[i].name.indexOf("VSCADA") !== -1) {
            deviceList[i].groups = [155];
            console.log(deviceList[i].name + " 155");
          }


          if (deviceList[i].connected == true)
          {
            if (deviceList[i].alarms.temperature_status.state == 1)
            {
              deviceList[i].issues = [
                {
                  "alertMessage": "High Temperature Detected",
                  "alertDate": "Today",
                  "alertTime": "All the time",
                  "alertLastReport": deviceList[i].alarms.temperature_status.ts
                }
              ];
            }
            else {
              deviceList[i].issues = "";
            }
          }

          else {
            deviceList[i].issues = [
              {
                "alertMessage": "Offline",
                "alertDate": "Today",
                "alertTime": "All the time",
                "alertLastReport": "Today"
              }
            ];
          }



        }
        //alert(deviceList[0].defName);

        //$this.titanium();
        $this.devices = deviceList;
        $rootScope.$broadcast('linesLoaded');

          },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log("")
      }
    });
  };



/*


  this.titanium= function(){
    //var proxy = '?callback=http://128.224.2.4:9090';
    var url = 'http://172.21.5.4:5000/v2.0/tokens';
    var dataBlock = {"auth":{"passwordCredentials":{"username": "hdcuser", "password": "hdcuser"}, "tenantName": "hdcdemo"}};
    $rootScope.instanceData;
    $.ajax({
      url: url,
      type: 'POST',
      ContentType: "application/json",
      data: JSON.stringify(dataBlock),
      beforeSend: function(xhr) {
        xhr.setRequestHeader('Content-Type' , 'application/json');
      },
      success: function(data, textStatus, jqXHR) {
        //alert(textStatus);
        $rootScope.instanceData = data;
        console.log($rootScope.instanceData.access.serviceCatalog[0].endpoints[0].publicURL);
      $this.serverDetails();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  };


*/


/*

this.serverDetails= function(){
    //var proxy = '?callback=http://128.224.2.4:9090';
    var url = '/servers/detail';

    $.ajax({
      url: $rootScope.instanceData.access.serviceCatalog[1].endpoints[0].publicURL + url,
      type: 'GET',
      ContentType: "application/json",
      beforeSend: function(xhr) {
        xhr.setRequestHeader('Content-Type' , 'application/json');
        xhr.setRequestHeader('X-Auth-Token' , $rootScope.instanceData.access.token.id);
      },
      success: function(data, textStatus, jqXHR) {
        //alert(textStatus);
        $scope.instanceData = data;


      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  };

  */


				this.setStatusOnDevices = function(devices) {
          var devicesObj = devices;
          //$scope.$apply();
            //console.log(devicesObj[0].connected);

            devicesObj.forEach(function(element) {
                if(element.connected == 'false') {
                  element.statusOffline = true; //offline state   element.statusOffline

                } else {
                  if (element.issues.length > 0) {
                    console.log("theres an alert");
                      element.statusAlert = true;   //alert state
                  } else {
                      element.statusAlert = false;
                  }
                  if (element.newUpdates > 0) {      //updates state, tbd put the check here instead of over there
                      element.statusUpdate = true;
                  } else {
                      element.statusUpdate = false;
                  }
                }
						});
						return devicesObj;
				};


				this.findDeviceAlerts = function(element) {
						return element.statusAlert === true;
				};
				this.findDeviceUpdates = function(element) {
						return element.statusUpdate === true;
				};

				/*
				 * Method to get a grouped list of devices
				 * @returns { Array of objects }
				 * assumes groups are part of this service
				 * outputs only groups with attached devices
				 */

				this.getGroupedDeviceList = function() {
          //console.log('getGroupedDeviceList')
						var deviceList = [];
						// Loop through list of Groups
						var x = 0;
								// console.log($this.devices);
						this.deviceGroups.forEach(function(group) {

								var deviceCnt = 0; //set var to count devices in group

								//set up array to adding the matched device to
								var tempArray = [];
								// Loop through list of devices to find some with groupId
								for (var i = 0; i < $this.devices.length; i++) {
										//if deviceGroup id is in device.groups property


										if ($this.devices[i].groups.indexOf(group.id) >= 0) {

                      //changed from groups.indexOf to group.groupId to deal with changes to API calls

												//push device to temp array
												tempArray.push($this.devices[i]);
												deviceCnt++; //and increment the device count
										}
								}
								//after device loop check device count > 0
								if (deviceCnt > 0) {
										deviceList[x] = group; //if true push group to grouped device list
										tempArray = $this.setStatusOnDevices(tempArray); //add status
										//set a group status if any child devices have alerts
										if (tempArray.some($this.findDeviceAlerts)) {

												deviceList[x].groupAlerts = true;

										} else {
												deviceList[x].groupAlerts = false;
										}
										if (tempArray.some($this.findDeviceUpdates)) {
												deviceList[x].groupUpdate = true;
										} else {
												deviceList[x].groupUpdate = false;
										}
										deviceList[x].devices = tempArray; //and add group of devices to list
										deviceList[x].deviceCount = deviceCnt; //set count property for output
										x++;
								}
								tempArray = null; //reset temp array

						});
						//return the completed grouped list
						// console.log(deviceList);
						return deviceList;
        };

				this.parseOsVersion = function(os) {

					var osArray = os.split(":");
					var result = [];

					for (var i = 0; i < osArray.length; i++) {
						if(osArray[i] !== "o" && osArray[i] !== "*") {
							result.push( osArray[i] );
						}
					}
					result = result.join(", ");
					return result;
				};

				/*
				 * Method to get data for one device
				 * @param {string} deviceId
				 * @returns {object} device
				 */
				this.getDevice = function(deviceId) {
					// console.log('getDevice:', deviceId);
						var device = null;
						var groupArr = [];

						for (var i = 0; i < $this.devices.length; i++) {
								if ($this.devices[i].id === deviceId) {
										device = $this.devices[i];
								}
						}
						if (device) {
								device.groups.forEach(function(groupId) {

                  //hardcoded for now
                  //groups to group.groupId

										$this.deviceGroups.forEach(function(group) {
                      //console.log("groupID: " + groupId );
                      //console.log("group.id: " + group.id );
												if (groupId === group.id) {
														groupArr.push(group.userFriendlyName);
												}
										});
								});
						}
						device.groupNames = groupArr;
						device.defName = $this.parseOsVersion(device.defName);

          return device;
				};


		});
